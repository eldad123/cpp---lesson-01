#include "Cell.h"
#include <string>
#include <iostream>

using std::cout;
using std::string;

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_refactor_gene = glucose_receptor_gene;
	Nucleus n;
	n.init(dna_sequence);

	this->__mitochondron = Mitochondrion();
	this->_nucleus = n;

	Ribosome r;
	this->_ribosome = r;
}

bool Cell::get_ATP()
{
	string rna =this->_nucleus.get_RNA_transcript(this->_glocus_refactor_gene);
	Protein* p = this->_ribosome.create_protein(rna);

	if (p == nullptr)
	{
		std::cerr << "Did not worked... " << std::endl;
		_exit(1);

	}
	else
	{
		this->__mitochondron.insert_glucose_receptor(*p);
		this->__mitochondron.set_glucose(50);
		bool atp = this->__mitochondron.produceATP();

		if (atp)
		{
			this->_atp_units = 100;
			return true;
		}
	}


	return false;
}


Nucleus Cell::get_nuclues()
{
	return this->_nucleus;
}

void Cell::set_nucleus(const Nucleus n)
{
	this->_nucleus = n;
}