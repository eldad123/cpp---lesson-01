#pragma once
#include "Nucleus.hpp"
#include "Ribosome.h"
#include "Mitochondrion.h"

class Cell
{
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();
	Nucleus get_nuclues();
	void set_nucleus(const Nucleus n);

private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion __mitochondron;
	Gene _glocus_refactor_gene;
	unsigned int _atp_units;
};

