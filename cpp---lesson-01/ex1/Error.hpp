#pragma once
#include <string>
#define DNA_SRAND_ERROR_CODE 001
using std::string;

class Error
{
public:
	Error(const int ERROR_CODE);

	string what_went_wrong() const { return this->error_string; }


private:
	string error_string;
};
