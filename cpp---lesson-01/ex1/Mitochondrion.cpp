#include "Mitochondrion.h"
#include "Protein.hpp"
#include <iostream>

using std::cout;
using std::endl;

Mitochondrion::Mitochondrion()
{
	this->_has_glocuse_refactor = false;
	this->_glocuse_level = 0;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	bool checkValid = false;
	AminoAcidNode* curr = protein.get_first();
	if (curr != nullptr && curr->get_data() == ALANINE)
	{
		curr = curr->get_next();
		if (curr != nullptr && curr->get_data() == LEUCINE)
		{
			curr = curr->get_next();
			if (curr != nullptr && curr->get_data() == GLYCINE)
			{

				curr = curr->get_next();
				if (curr != nullptr && curr->get_data() == HISTIDINE)
				{
					curr = curr->get_next();
					if (curr != nullptr && curr->get_data() == LEUCINE)
					{
						curr = curr->get_next();
						if (curr != nullptr && curr->get_data() == PHENYLALANINE)
						{
							curr = curr->get_next();
							if (curr != nullptr && curr->get_data() == AMINO_CHAIN_END)
							{
								checkValid = true;
							}

						}
					}
				}

			}
		}
	}

	if (checkValid)
	{
		this->_has_glocuse_refactor = true;
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	bool checkPosible = false;

	if (this->_glocuse_level >= 50 && this->_has_glocuse_refactor)
		checkPosible = true;
		
	return true;
}
