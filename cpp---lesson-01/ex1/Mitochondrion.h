#pragma once
#pragma once
#pragma once
#include <string>
#include "Protein.hpp"
using std::string;


class Mitochondrion
{
public:
	//c'tor
	Mitochondrion();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;

private:
	unsigned int _glocuse_level;
	bool _has_glocuse_refactor;


};


