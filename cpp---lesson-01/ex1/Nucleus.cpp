#include "Nucleus.hpp"
#include "Error.hpp"
#include <string>
#include <algorithm>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;

}

unsigned int Gene::get_start()const
{
	return this->_start;
}
unsigned int Gene::get_end()const
{
	return this->_end;
}

unsigned int Gene::is_on_complementary_dna_strand()const
{
	return this->_on_complementary_dna_strand;
}

void Gene::set_start(const int start)
{
	this->_start = start;
}

void Gene::set_end(const int end)
{
	this->_end = end; 
}
void Gene::set_on_complementary_dna_strand(const int on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Nucleus::init(const string DNA_strand)
{
	this->_complementary_DNA_strand = "";
	this->_DNA_strand = DNA_strand;

	//check valid DNA
	bool valid_input = true;
	for (size_t i = 0; i < DNA_strand.length() && valid_input; i++)
	{
		if (!(DNA_strand[i] == 'A' || DNA_strand[i] == 'G' ||
			DNA_strand[i] == 'T' || DNA_strand[i] == 'C'))
		{
			valid_input = false;
			throw(Error(DNA_SRAND_ERROR_CODE));
		}
	}


	if (valid_input)
	{
		for (size_t i = 0; i < DNA_strand.length(); i++)
		{
			switch (DNA_strand[i])
			{
			case 'A':
				this->_complementary_DNA_strand += 'T';
				break;

			case 'G':
				this->_complementary_DNA_strand += 'C';
				break;

			case 'C':
				this->_complementary_DNA_strand += 'G';
				break;

			case 'T':
				this->_complementary_DNA_strand += 'A';
				break;


			default:
				break;
			}
		}
	}

}


string Nucleus::get_complementary_DNA_strand() const
{
	return this->_complementary_DNA_strand;
}

string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string dna = "";
	string rna = "";
	if(gene.is_on_complementary_dna_strand())
	{ 
		dna = this->_complementary_DNA_strand;
	}
	else
	{
		dna = this->_DNA_strand;
	}

	for (size_t i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (dna[i] == 'T')
			rna += "U";
		else rna += dna[i];
	}

	return rna;
}

string Nucleus::get_reversed_DNA_strand() const
{
	string reverse_string(this->_DNA_strand);
	std::reverse(reverse_string.begin(), reverse_string.end());
	return reverse_string;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	if (codon.length() == 0) return 0;
	unsigned int count = 0;
	for (size_t offset = this->_DNA_strand.find(codon); offset != std::string::npos;
		offset = this->_DNA_strand.find(codon, offset + codon.length()))
	{
		count++;
	}
	return count;

}


string Nucleus::get_DNA_strand() const
{
	return this->_DNA_strand;
}

string Nucleus::get_DNA_strand_from_RNA(const string RNA)
{
	string dna_strand = "";
	for (size_t i = 0; i < RNA.length(); i++)
	{
		if (RNA[i] == 'U')
		{
			dna_strand += "T";
		}
		else
		{
			dna_strand += RNA[i];
		}
	}

	return dna_strand;
}

