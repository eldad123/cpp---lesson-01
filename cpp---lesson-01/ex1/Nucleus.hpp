#pragma once
#include <string>
#include "Protein.hpp"
using std::string;

class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	unsigned int get_start()const; //Getters
	unsigned int get_end()const;
	unsigned int is_on_complementary_dna_strand()const;

	void set_start(const int start); //Setters
	void set_end(const int end);
	void set_on_complementary_dna_strand(const int on_complementary_dna_strand);



private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;



};


class Nucleus
{
public:
	void init(const string _DNA_strand);
	string get_complementary_DNA_strand() const;
	string get_RNA_transcript(const Gene& gene) const;
	string get_DNA_strand() const;
	string get_DNA_strand_from_RNA(const string RNA);
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

private:
	string _DNA_strand;
	string _complementary_DNA_strand;


};

