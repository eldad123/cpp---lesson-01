#include "Ribosome.h"
#include "AminoAcid.hpp"
#include <string>
#include "Protein.hpp"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;


Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	string codon;
	AminoAcid aminoAcid = ALANINE;
	Protein* p = new Protein;
	p->init();
	
	while (aminoAcid != UNKNOWN && RNA_transcript.length() >= 3)
	{
		try {
			codon = RNA_transcript.substr(0, 3);
			if (RNA_transcript.length() >= 3)   RNA_transcript = RNA_transcript.substr(3);
		}
		catch (...)
		{
			std::cerr << "Substr error"<< endl;
			_exit(1);
		}


		aminoAcid = get_amino_acid(codon);
		if (aminoAcid == UNKNOWN)
		{
			p->~LinkedList();
			return nullptr;
				
		}
		else
		{
			p->add(aminoAcid);
		}

	}


	return p;
}
