#pragma once
#include "Protein.hpp"

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;

};

