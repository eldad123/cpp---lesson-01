#include <iostream>

using std::cout;
using std::endl;

void s(int x, int& result)
{
	result = x * x;
}
int& getDynamicVar()
{
	int* x = new int(10);
	return *x;
}


int& getLocalVar()
{
	int x = 10;
	return x;

}
int main()
{
	int x = 7;
	int& r_x = getLocalVar();

	cout << r_x << endl;
	int y = getDynamicVar();

	cout << y << endl;

	s(3, y);

	cout << y<<endl;


	return 0;
}