#include "Virus.h"
#include "Nucleus.hpp"
#include <string>
#include <iostream>

using std::string;

Virus::Virus(const string RNA_seqeunce)
{
	this->_RNA_sequence = RNA_seqeunce;
}


void Virus::infect_cell(Cell& cell) const
{
	string virus_dna = cell.get_nuclues().get_DNA_strand_from_RNA(this->_RNA_sequence);

	string old_dna = cell.get_nuclues().get_DNA_strand();
	int middle_pos = old_dna.length() / 2;

	string new_dna = old_dna.substr(0, middle_pos) + virus_dna + old_dna.substr(middle_pos);

	Nucleus n;
	n.init(new_dna);

	cell.set_nucleus(n);
}