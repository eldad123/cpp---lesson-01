#pragma once
#include "Cell.h"
#include <string>

using std::string;

class Virus
{
public:
	Virus(const string RNA_seqeunce);
	void infect_cell(Cell& cell) const;

private:
	string _RNA_sequence;
};

